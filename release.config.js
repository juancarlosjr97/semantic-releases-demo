module.exports = {
  branches: [
    { name: "main" },
    { name: "nonprod", channel: "nonprod", prerelease: "rc" }, // `prerelease` is built with the template `${name.replace(/^pre\\//g, "")}`
    { name: "develop", channel: "develop", prerelease: true }, // `prerelease` is set to `beta` as it is the value of `name`
  ],
  repositoryUrl: "https://gitlab.com/juancarlosjr97/semantic-releases-demo",
  plugins: [
    [
      "@semantic-release/commit-analyzer",
      {
        preset: "angular",
        releaseRules: [
          {
            type: "docs",
            release: "patch",
          },
          {
            type: "refactor",
            release: "patch",
          },
        ],
      },
    ],
    [
      "@semantic-release/release-notes-generator",
      {
        preset: "conventionalcommits",
        presetConfig: {
          types: [
            {
              type: "feat",
              section: "Features",
            },
            {
              type: "fix",
              section: "Bug Fixes",
            },
            {
              type: "perf",
              section: "Performance Improvements",
            },
            {
              type: "revert",
              section: "Reverts",
            },
            {
              type: "docs",
              section: "Documentation",
            },
            {
              type: "refactor",
              section: "Code Refactoring",
            },
          ],
        },
      },
    ],
    [
      "@semantic-release/changelog",
      {
        changelogFile: "CHANGELOG.md",
        changelogTitle:
          "# Changelog\nThis changelog is auto generated using semantic releases.",
      },
    ],
    "@semantic-release/npm",
    [
      "@semantic-release/git",
      {
        assets: ["package.json", "CHANGELOG.md"],
        message:
          "chore(release): ${nextRelease.version}\n\n${nextRelease.notes}",
      },
    ],
  ],
};
