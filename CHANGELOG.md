# Changelog
This changelog is auto generated using semantic releases.

## [1.5.0](https://gitlab.com/juancarlosjr97/semantic-releases-demo/compare/v1.4.0...v1.5.0) (2023-03-28)


### Features

* updating documentation to test signed commits ([95027c5](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/95027c52cd260de2c3cbe670388260dfabc74006))

## [1.4.0](https://gitlab.com/juancarlosjr97/semantic-releases-demo/compare/v1.3.0...v1.4.0) (2022-07-10)


### Features

* testing signed commits ([832d347](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/832d3474535e7b6b6f5ee6e5c1ffebead2675f0d))
* testing signed commits ([2a9d117](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/2a9d117633ca52f3d08b50cb913d22c68d269dc5))
* testing signed commits ([a0c504e](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/a0c504ef2ba1a0e7cc5e378330c1dc99438e75de))
* testing signed commits ([d44142e](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/d44142eccae1d50067592e69bf05b199cd2921d8))
* testing signed commits ([3ec9d4f](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/3ec9d4f55b1d44e78f06f9a949f3af9d5f540a38))
* testing signed commits ([311b27f](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/311b27ff48765a6d91fc23c3b5274a9cf9e52a19))
* testing signed commits ([38da780](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/38da780e548e962aaf897c848db07de80aca43de))

## [1.3.0](https://gitlab.com/juancarlosjr97/semantic-releases-demo/compare/v1.2.1...v1.3.0) (2022-05-25)


### Features

* removing gitlab semantic release plugin ([1112793](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/1112793ec66be3dee5c1b521d05adb3337ffa246))

### [1.2.1](https://gitlab.com/juancarlosjr97/semantic-releases-demo/compare/v1.2.0...v1.2.1) (2022-04-13)


### Documentation

* updating for nonprod releases ([f049d78](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/f049d78ba0f868cae324e03de30f2864ec3dc963))

### [1.2.1-rc.1](https://gitlab.com/juancarlosjr97/semantic-releases-demo/compare/v1.2.0...v1.2.1-rc.1) (2022-04-13)


### Documentation

* updating for nonprod releases ([f049d78](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/f049d78ba0f868cae324e03de30f2864ec3dc963))

## [1.2.0](https://gitlab.com/juancarlosjr97/semantic-releases-demo/compare/v1.1.0...v1.2.0) (2022-04-13)


### Features

* adding release for nonprod and develop ([9c35b32](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/9c35b324934c493356c31add53d0763b7fe7a952))


### Bug Fixes

* fixing wrong branch as head ([ae36756](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/ae36756bfae11632812d0c2430faa5671580c069))

## [1.1.0](https://gitlab.com/juancarlosjr97/semantic-releases-demo/compare/v1.0.0...v1.1.0) (2022-04-13)


### Features

* adding environment by branch ([dc9d7ff](https://gitlab.com/juancarlosjr97/semantic-releases-demo/commit/dc9d7ff21d5c4d07df12e5d62aa560704a053191))
