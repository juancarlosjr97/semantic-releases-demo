module.exports = {
  extends: ["@commitlint/config-conventional"],
  ignores: [(commitMessage) => commitMessage.includes("chore(release)")],
};
