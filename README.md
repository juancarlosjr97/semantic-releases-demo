# Semantic Releases GitLab Example

[![pipeline status](https://gitlab.com/juancarlosjr97/semantic-releases-demo/badges/main/pipeline.svg)](https://gitlab.com/juancarlosjr97/semantic-releases-demo/-/commits/main)

This repository contains an example of how to use GitLab for `semantic-releases` using GitLab CI.

## Features

- [x] `CHANGELOG.md` automation
- [x] `package.json` version increase
- [x] Creating tag and releases
- [x] Creating pre-releases
- [x] Creating nonprod release

## Instructions

1. Adding `GITLAB_TOKEN` as a environmental variable to the repository
